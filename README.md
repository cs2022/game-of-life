# Game Of Life

## Ruleset
The domain is periodic i.e. the right neighbor of the rightmost cells is the leftmost cell. For the other boundaries this applies analogously.

* Any live cell with fewer than two live neighbors dies, as if by underpopulation.
* Any live cell with two or three live neighbors lives on to the next generation.
* Any live cell with more than three live neighbors dies, as if by overpopulation.
* Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
